package br.com.jorgerabellodev.customer.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer")
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private  Long id;

  @Column(name = "name", length = 50, nullable = false)
  private  String name;

  @Column(name = "surname", length = 100, nullable = false)
  private  String surname;

  @Column(name = "cpf", length = 11, unique = true, nullable = true)
  private  String cpf;

}
