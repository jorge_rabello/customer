package br.com.jorgerabellodev.customer.domain.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class CustomerResponse {

  private final Long id;
  private final String name;
  private final String surname;
  private final String cpf;

}
