package br.com.jorgerabellodev.customer.api.handler;

import br.com.jorgerabellodev.customer.api.errors.details.ExceptionDetails;
import br.com.jorgerabellodev.customer.api.errors.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
    ExceptionDetails details = ExceptionDetails
        .builder()
        .message(resourceNotFoundException.getMessage())
        .build();
    return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    ExceptionDetails details = ExceptionDetails
        .builder()
        .message(ex.getMessage())
        .build();
    return new ResponseEntity<>(details, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex,
      Object body,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    ExceptionDetails details = ExceptionDetails
        .builder()
        .message(ex.getMessage())
        .build();
    return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<?> handleException(Exception exception) {
    ExceptionDetails details = ExceptionDetails
        .builder()
        .message(exception.getMessage())
        .build();
    return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
