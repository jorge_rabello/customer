package br.com.jorgerabellodev.customer.api.repository;

import br.com.jorgerabellodev.customer.domain.entities.Customer;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

  Optional<Customer> findByCpf(String cpf);
}
