package br.com.jorgerabellodev.customer.api.endpoint;

import br.com.jorgerabellodev.customer.api.service.CustomerService;
import br.com.jorgerabellodev.customer.domain.dto.CustomerRequest;
import br.com.jorgerabellodev.customer.domain.dto.CustomerResponse;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/customer")
public class CustomerEndpoint {

  private final CustomerService service;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAll() {
    List<CustomerResponse> allCustomers = service.findAll();
    return new ResponseEntity<>(allCustomers, HttpStatus.OK);
  }

  @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findById(@PathVariable("id") Long id) {
    CustomerResponse customer = service.findById(id);
    return new ResponseEntity<>(customer, HttpStatus.OK);
  }

  @GetMapping(value = "/findByCpf/{cpf}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findByCPF(@PathVariable("cpf") String cpf) {
    CustomerResponse customer = service.findByCpf(cpf);
    return new ResponseEntity<>(customer, HttpStatus.OK);
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> newCustomer(@RequestBody CustomerRequest customerRequest) {
    CustomerResponse savedCustomer = service.save(customerRequest);
    return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
  }

  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> updateCustomer(@RequestBody CustomerRequest customerRequest, @PathVariable("id") Long id) {
    CustomerResponse updatedCustomer = service.update(customerRequest, id);
    return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
