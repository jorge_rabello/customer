package br.com.jorgerabellodev.customer.api.service;

import br.com.jorgerabellodev.customer.api.errors.exceptions.ResourceNotFoundException;
import br.com.jorgerabellodev.customer.api.repository.CustomerRepository;
import br.com.jorgerabellodev.customer.domain.dto.CustomerRequest;
import br.com.jorgerabellodev.customer.domain.dto.CustomerResponse;
import br.com.jorgerabellodev.customer.domain.entities.Customer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {

  private final CustomerRepository repository;

  public List<CustomerResponse> findAll() {
    List<Customer> allCustomers = repository.findAll();
    List<CustomerResponse> allCustomersResponse = new ArrayList<>();
    allCustomers.forEach(customer -> {
      CustomerResponse customerResponse = CustomerResponse
          .builder()
          .id(customer.getId())
          .name(customer.getName())
          .surname(customer.getSurname())
          .cpf(customer.getCpf())
          .build();
      allCustomersResponse.add(customerResponse);
    });
    return allCustomersResponse;
  }

  public CustomerResponse findById(Long id) {
    Optional<Customer> customer = repository.findById(id);
    if (!customer.isPresent()) {
      throw new ResourceNotFoundException("Customer not found for id: " + id);
    }
    return CustomerResponse
        .builder()
        .id(customer.get().getId())
        .name(customer.get().getName())
        .surname(customer.get().getSurname())
        .cpf(customer.get().getCpf())
        .build();
  }

  public CustomerResponse findByCpf(String cpf) {
    Optional<Customer> customer = repository.findByCpf(cpf);
    if (!customer.isPresent()) {
      throw new ResourceNotFoundException("Customer not found for cpf: " + cpf);
    }
    return CustomerResponse
        .builder()
        .id(customer.get().getId())
        .name(customer.get().getName())
        .surname(customer.get().getSurname())
        .cpf(customer.get().getCpf())
        .build();
  }

  public CustomerResponse save(CustomerRequest customerRequest) {
    Customer customer = Customer
        .builder()
        .name(customerRequest.getName())
        .surname(customerRequest.getSurname())
        .cpf(customerRequest.getCpf())
        .build();
    Customer savedCustomer = repository.save(customer);
    return CustomerResponse
        .builder()
        .id(savedCustomer.getId())
        .name(savedCustomer.getName())
        .surname(savedCustomer.getSurname())
        .cpf(savedCustomer.getCpf())
        .build();
  }

  public CustomerResponse update(CustomerRequest customerRequest, Long id) {
    Optional<Customer> optionalCustomer = repository.findById(id);
    if (!optionalCustomer.isPresent()) {
      throw new ResourceNotFoundException("Customer not found by id: "+ id);
    }
    Customer customer = Customer
        .builder()
        .id(id)
        .name(customerRequest.getName())
        .surname(customerRequest.getSurname())
        .cpf(customerRequest.getCpf())
        .build();
    Customer updatedCustomer = repository.save(customer);
    return CustomerResponse
        .builder()
        .id(updatedCustomer.getId())
        .name(updatedCustomer.getName())
        .surname(updatedCustomer.getSurname())
        .cpf(updatedCustomer.getCpf())
        .build();
  }

  public void delete(Long id) {
    Optional<Customer> optionalCustomer = repository.findById(id);
    if (!optionalCustomer.isPresent()) {
      throw new ResourceNotFoundException("Customer not found by id: "+ id);
    }
    repository.deleteById(id);
  }
}
