# CUSTOMER SERVICE

## BUILD
```
mvn clean install
```

## MS SQL SERVER DOCKER COMPOSE START
```
docker-compose up -d
```

## MS SQL SERVER DOCKER COMPOSE CHECK
```
        Name                       Command               State            Ports         
----------------------------------------------------------------------------------------
customer_sqlserver1_1   /bin/sh -c /opt/mssql/bin/ ...   Up      0.0.0.0:15789->1433/tcp

```

## MS SQL SERVER DOCKER COMPOSE START
```
docker-compose down
```
